<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190906170807 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE genre (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(191) NOT NULL, UNIQUE INDEX UNIQ_835033F85E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media_video (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(191) NOT NULL, identif VARCHAR(191) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commentaire (id INT AUTO_INCREMENT NOT NULL, film_id INT NOT NULL, user_id INT NOT NULL, contenu TINYTEXT DEFAULT NULL, note INT DEFAULT NULL, INDEX IDX_67F068BC567F5183 (film_id), INDEX IDX_67F068BCA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE film (id INT AUTO_INCREMENT NOT NULL, media_video_id INT DEFAULT NULL, authered_id INT DEFAULT NULL, titre VARCHAR(191) NOT NULL, annee DATE DEFAULT NULL, date_submited DATETIME DEFAULT NULL, lien VARCHAR(191) DEFAULT NULL, note DOUBLE PRECISION DEFAULT NULL, nb_coms INT DEFAULT NULL, UNIQUE INDEX UNIQ_8244BE2226A02EFC (media_video_id), INDEX IDX_8244BE22A70ED4DC (authered_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE film_realisateur (film_id INT NOT NULL, realisateur_id INT NOT NULL, INDEX IDX_3F2B13F1567F5183 (film_id), INDEX IDX_3F2B13F1F1D8422E (realisateur_id), PRIMARY KEY(film_id, realisateur_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filmsavoir_users (film_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_7CF96A8C567F5183 (film_id), INDEX IDX_7CF96A8CA76ED395 (user_id), PRIMARY KEY(film_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE filmsvus_users (film_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_1254AE08567F5183 (film_id), INDEX IDX_1254AE08A76ED395 (user_id), PRIMARY KEY(film_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE film_genre (film_id INT NOT NULL, genre_id INT NOT NULL, INDEX IDX_1A3CCDA8567F5183 (film_id), INDEX IDX_1A3CCDA84296D31F (genre_id), PRIMARY KEY(film_id, genre_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(191) NOT NULL, prenom VARCHAR(191) DEFAULT NULL, nom VARCHAR(191) DEFAULT NULL, mail VARCHAR(191) NOT NULL, password VARCHAR(191) NOT NULL, token VARCHAR(191) NOT NULL, token_validity DATETIME NOT NULL, salt VARCHAR(191) DEFAULT NULL, is_active TINYINT(1) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', last_activity DATETIME NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), UNIQUE INDEX UNIQ_8D93D6495126AC48 (mail), UNIQUE INDEX UNIQ_8D93D6495F37A13B (token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE realisateur (id INT AUTO_INCREMENT NOT NULL, nom_complet VARCHAR(191) NOT NULL, UNIQUE INDEX UNIQ_47933EFE2FAD34A2 (nom_complet), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE commentaire ADD CONSTRAINT FK_67F068BC567F5183 FOREIGN KEY (film_id) REFERENCES film (id)');
        $this->addSql('ALTER TABLE commentaire ADD CONSTRAINT FK_67F068BCA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE film ADD CONSTRAINT FK_8244BE2226A02EFC FOREIGN KEY (media_video_id) REFERENCES media_video (id)');
        $this->addSql('ALTER TABLE film ADD CONSTRAINT FK_8244BE22A70ED4DC FOREIGN KEY (authered_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE film_realisateur ADD CONSTRAINT FK_3F2B13F1567F5183 FOREIGN KEY (film_id) REFERENCES film (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE film_realisateur ADD CONSTRAINT FK_3F2B13F1F1D8422E FOREIGN KEY (realisateur_id) REFERENCES realisateur (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filmsavoir_users ADD CONSTRAINT FK_7CF96A8C567F5183 FOREIGN KEY (film_id) REFERENCES film (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filmsavoir_users ADD CONSTRAINT FK_7CF96A8CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filmsvus_users ADD CONSTRAINT FK_1254AE08567F5183 FOREIGN KEY (film_id) REFERENCES film (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE filmsvus_users ADD CONSTRAINT FK_1254AE08A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE film_genre ADD CONSTRAINT FK_1A3CCDA8567F5183 FOREIGN KEY (film_id) REFERENCES film (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE film_genre ADD CONSTRAINT FK_1A3CCDA84296D31F FOREIGN KEY (genre_id) REFERENCES genre (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE film_genre DROP FOREIGN KEY FK_1A3CCDA84296D31F');
        $this->addSql('ALTER TABLE film DROP FOREIGN KEY FK_8244BE2226A02EFC');
        $this->addSql('ALTER TABLE commentaire DROP FOREIGN KEY FK_67F068BC567F5183');
        $this->addSql('ALTER TABLE film_realisateur DROP FOREIGN KEY FK_3F2B13F1567F5183');
        $this->addSql('ALTER TABLE filmsavoir_users DROP FOREIGN KEY FK_7CF96A8C567F5183');
        $this->addSql('ALTER TABLE filmsvus_users DROP FOREIGN KEY FK_1254AE08567F5183');
        $this->addSql('ALTER TABLE film_genre DROP FOREIGN KEY FK_1A3CCDA8567F5183');
        $this->addSql('ALTER TABLE commentaire DROP FOREIGN KEY FK_67F068BCA76ED395');
        $this->addSql('ALTER TABLE film DROP FOREIGN KEY FK_8244BE22A70ED4DC');
        $this->addSql('ALTER TABLE filmsavoir_users DROP FOREIGN KEY FK_7CF96A8CA76ED395');
        $this->addSql('ALTER TABLE filmsvus_users DROP FOREIGN KEY FK_1254AE08A76ED395');
        $this->addSql('ALTER TABLE film_realisateur DROP FOREIGN KEY FK_3F2B13F1F1D8422E');
        $this->addSql('DROP TABLE genre');
        $this->addSql('DROP TABLE media_video');
        $this->addSql('DROP TABLE commentaire');
        $this->addSql('DROP TABLE film');
        $this->addSql('DROP TABLE film_realisateur');
        $this->addSql('DROP TABLE filmsavoir_users');
        $this->addSql('DROP TABLE filmsvus_users');
        $this->addSql('DROP TABLE film_genre');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE realisateur');
    }
}
