<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210706193204 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE commentaire CHANGE note note INT DEFAULT NULL, CHANGE date_submitted date_submitted DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE film ADD information LONGTEXT DEFAULT NULL, CHANGE media_video_id media_video_id INT DEFAULT NULL, CHANGE authered_id authered_id INT DEFAULT NULL, CHANGE annee annee DATE DEFAULT NULL, CHANGE date_submited date_submited DATETIME DEFAULT NULL, CHANGE lien lien VARCHAR(191) DEFAULT NULL, CHANGE note note DOUBLE PRECISION DEFAULT NULL, CHANGE nb_coms nb_coms INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE prenom prenom VARCHAR(191) DEFAULT NULL, CHANGE nom nom VARCHAR(191) DEFAULT NULL, CHANGE salt salt VARCHAR(191) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE commentaire CHANGE note note INT DEFAULT NULL, CHANGE date_submitted date_submitted DATETIME DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE film DROP information, CHANGE media_video_id media_video_id INT DEFAULT NULL, CHANGE authered_id authered_id INT DEFAULT NULL, CHANGE annee annee DATE DEFAULT \'NULL\', CHANGE date_submited date_submited DATETIME DEFAULT \'NULL\', CHANGE lien lien VARCHAR(191) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE note note DOUBLE PRECISION DEFAULT \'NULL\', CHANGE nb_coms nb_coms INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE prenom prenom VARCHAR(191) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE nom nom VARCHAR(191) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`, CHANGE salt salt VARCHAR(191) CHARACTER SET utf8mb4 DEFAULT \'NULL\' COLLATE `utf8mb4_unicode_ci`');
    }
}
