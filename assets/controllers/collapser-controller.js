import { Controller } from '@hotwired/stimulus';

export default class extends Controller {
    static targets = ['collapse', 'button']
    connect() {
        this.collapseTarget.addEventListener ('hide.bs.collapse', () => {
            this.buttonTarget.classList.remove("fa-rotate-90");
        });
        this.collapseTarget.addEventListener ('show.bs.collapse', () => {
            this.buttonTarget.classList.add("fa-rotate-90");
        })
    }
}