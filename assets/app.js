import $ from 'jquery';
global.$ = $;

import './styles/app.scss';
import 'bootstrap';
import './bootstrap';
import 'font-awesome/css/font-awesome.css';
import 'jquery-ui/themes/base/all.css';
import 'jquery-ui/ui/widgets/autocomplete';
import 'bootstrap-star-rating';
import 'bootstrap-star-rating/locales/fr'
import 'bootstrap-star-rating/js/locales/fr'
import 'bootstrap-star-rating/css/star-rating.css';
import 'bootstrap-star-rating/themes/krajee-fa/theme';
import 'bootstrap-star-rating/themes/krajee-fa/theme.css';
import './js/suivifilms2';
import './js/addCollectionWidget';
import './js/filtre';

jQuery(function() {
    $('.add-another-collection-widget').addCollection();

    $('[data-fonction="switch"]').switchEtat();

    $('[data-toggle="star-filter"]').filtreParNote();
})