(function ($) {
    $.fn.filtre = function () {
        let value = $(this).val().toLowerCase();
        let target = $(this).data('target');
        $(target + " article").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    };
}) (jQuery);

(function ($) {
    $.fn.filtreParNote = function () {
        this.each(function() {
            $(this).val("0");
            $(this).on("change", function(e) {
                let value = $(this).val();
                let target = $(this).data('path');
                if ($(target + " article").length) {
                    $(target + " article").filter(function () {
                        $(this).toggle($("input.rating", this).val() >= value);
                    });
                } else {
                    $(target + "  tr").filter(function () {
                        $(this).toggle($("td input.rating", this).val() >= value);
                    });
                }
            });
        });
    }
}) (jQuery);