(function ($) {
    $.fn.switchEtat = function () {
        this.each(function() {
            $(this).on("click", function(e) {
                e.preventDefault();
                let $icone = $(this).children('i');
                let contenu = $(this).data('content');
                let iconeEtatUn = $(this).data('icone-actif');
                let iconeEtatDeux = $(this).data('icone-inactif');
                let chemin = $(this).data('path');
                $.ajax({
                    type: 'POST',
                    url: chemin,
                    data: 'id_film=' + contenu,
                    success: function (data) {
                        if ($icone.attr('class') === iconeEtatUn) {
                            $icone.attr('class', iconeEtatDeux);
                        } else {
                            $icone.attr('class', iconeEtatUn);
                        }
                        return;
                    },
                    complete: function () {

                    }
                })
            })
        })
    }
})(jQuery);