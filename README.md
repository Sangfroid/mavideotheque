Vidéothèque partagée
===

Le principe de cette vidéothèque n'est pas de recréer une immense base de données mais de partager avec ses amis des films que l'on vient de voir ou que l'on souhaite voir.

La liste complète des films est accessible publiquement. Il faut par contre se créer un compte pour ajouter, commenter des films.

Depuis un compte enregistré, on peut :
* ajouter un film
* ajout une note et un commentaire sur un film
* Gérer la liste des films que l'on souhaite voir
* Cocher un film que l'on a vu

Technique
===

Le projet s'appuie sur Symfony 6.1 et utilise Bootstrap 5, JQuery, JQuery-ui, Font-Awesome, bootstrap-star-rating et Yarn.

La partie intégration de vidéos et presque intégralement reprise de https://www.zirolis.com/tuto/integrer-une-video-externe-avec-symfony-et-php

Le service Mattermost permet d'envoyer une notif à l'aide d'un webhook entrant généré sur un serveur mattermost.

Installation
===

Nécessite curl et sudo

```
$ git clone https://gogs.fdlibre.eu/sangfroid/films-symfony-4-2.git
$ cd ./films-symfony-4-2
```

Copier le fichier .env en .env.dev.local pour le développement, puis paramétrer le fichier.

**EN PRODUCTION** : Créer un fichier .env de production
```
$ composer dump-env prod
```
Puis modifier le fichier avec ses propres paramètres.


Mettre à jour les dépendances :
```
$ composer install
```

Mettre à jour les dépendances yarn

**EN DÉVELOPPEMENT** :
```
$ yarn install
$ yarn encore dev
```
**EN PRODUCTION** :
```
$ yarn install
$ yarn encore production
```

Créer la base de données et créer un utilisateur
```
$ php bin/console doctrine:database:create
$ php bin/console doctrine:schema:create
$ php bin/console app:user:create
```

Mise à jour
===

```
$ cd /path_to/symfony-4-2/
$ git pull
$ yarn install
$ composer install
$ yarn encore production
$ php bin/console doctrine:migrations:migrate
$ php bin/console cache:clear --env=prod
```

Exemple de Vhost Apache
===

Ceci est un exemple pour un développement local simple

```apache
<VirtualHost *:80>
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/symfony-4-2/public
        ServerName films.localdomain

        <Directory /var/www/symfony-4-2/public>
                AllowOverride All
                Order Allow,Deny
                Allow from All
        </Directory>
</VirtualHost>
```

