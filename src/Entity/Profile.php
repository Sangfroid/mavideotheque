<?php

namespace App\Entity;

use App\Repository\ProfileRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProfileRepository::class)]
class Profile
{
    public static $VIEW = ['liste' => 0, 'vignette' => 1];


    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: "integer")]

    private ?int $id = null;


    #[ORM\OneToOne(targetEntity: User::class, inversedBy: "profile", cascade: ["persist", "remove"])]
    #[ORM\JoinColumn(nullable: false)]

    private ?User $user = null;


    #[ORM\Column(type: "integer")]

    private ?int $view = null;

    public function __construct()
    {
        $this->setView(Profile::$VIEW['vignette']);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getView(): ?int
    {
        return $this->view;
    }

    public function setView(?int $view): self
    {
        $this->view = $view;

        return $this;
    }
}
