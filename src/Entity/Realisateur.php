<?php

namespace App\Entity;

use App\Repository\RealisateurRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: "realisateur")]
#[ORM\Entity(repositoryClass: RealisateurRepository::class)]

class Realisateur
{

    #[ORM\Column(name: "id", type: "integer")]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "AUTO")]
    
    private ?int $id = null;

    #[ORM\Column(name: "nom_complet", type: "string", length: 191, unique: true)]
    
    private ?string $nomComplet = null;

    #[ORM\ManyToMany(targetEntity: Film::class, mappedBy: "realisateurs", cascade: ["persist"])]
    
    private ?Collection $films = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->films = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addFilm(Film $film): self
    {
        $this->films[] = $film;

        return $this;
    }

    public function removeFilm(Film $film): self
    {
        $this->films->removeElement($film);

        return $this;
    }

    public function getFilms(): Collection
    {
        return $this->films;
    }

    public function getNomComplet (): ?string
    {
        return $this->nomComplet;
    }

    public function setNomComplet(?string $nom): self
    {
        $this->nomComplet = $nom;

        return $this;
    }

}
