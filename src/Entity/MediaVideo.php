<?php

namespace App\Entity;

use App\Repository\MediaVideoRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Table(name: "media_video")]
#[ORM\Entity(repositoryClass: MediaVideoRepository::class)]
#[ORM\HasLifecycleCallbacks] // Permet d’utiliser des événements

class MediaVideo
{
    #[ORM\Column(name: "id", type: "integer")]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "AUTO")]
    
    private ?int $id = null;

    #[ORM\Column(name: "type", type: "string", length: 191)]
    
    private ?string $type = null;

    #[ORM\Column(name: "identif", type: "string", length: 191)]
    
    private ?string $identif = null;

    #[Assert\Regex(
        pattern: "#^(http|https)://(youtu.be|www.youtube.com|www.dailymotion.com|vimeo.com|video.fdlibre.eu)/#",
        match: true,
        message: "L'url doit correspondre à l'url d'une vidéo Youtube, DailyMotion, Vimeo ou Peertube(fdlibre)"
    )]
    
    private ?string $url = null;

    public function setFromTmdb(string $type, string $identif): void
    {
        $this->setType($type);
        $this->setIdentif($identif);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url();
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;
        $this->extractIdentif();

        return $this;
    }
    
    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }
    
    public function setIdentif(?string $identif): self
    {
        $this->identif = $identif;

        return $this;
    }
    
    public function getIdentif(): ?string
    {
        return $this->identif;
    }

    /////////////////////
    // Méthodes
    private function peertubeId(string $url): void
    {
        $tableau = explode("/", $url);
        $id = $tableau[count($tableau)-1];
        $this->setIdentif($id);
        $this->setType('peertube');
    }
    
    private function youtubeId(string $url): void
    {
        $tableaux = explode("=", $url);  // découpe l’url en deux  avec le signe ‘=’

        $this->setIdentif($tableaux[1]);  // ajoute l’identifiant à l’attribut identif
        $this->setType('youtube');  // signale qu’il s’agit d’une video youtube et l’inscrit dans l’attribut $type
    }

    private function youtubeCourteId(string $url): void
    {
        $tableaux = explode("/", $url);  // on découpe l’url grâce au « / »
        $id = $tableaux[count($tableaux)-1];  // on retient la dernière partie qui contient l’identifiant
        $this->setIdentif($id);  // ajoute l’identifiant à l’attribut identif
        $this->setType('youtube');  // signale qu’il s’agit d’une video youtube et l’inscrit dans l’attribut $type
    }

    private function dailymotionId(string $url): void
    {
        $cas = explode("/", $url); // On sépare la première partie de l'url des 2 autres
        $idb = $cas[4];  // On récupère la partie qui nous intéressent
        $bp = explode("_", $idb);  // On sépare l'identifiant du reste
        $id = $bp[0]; // On récupère l'identifiant

        $this->setIdentif($id);  // ajoute l’identifiant à l’attribut identif
        $this->setType('dailymotion'); // signale qu’il s’agit d’une video dailymotion et l’inscrit dans l’attribut $type
    }

    private function vimeoId(string $url): void
    {
        $tableaux = explode("/", $url);  // on découpe l’url grâce au « / »
        $id = $tableaux[count($tableaux)-1];  // on reticent la dernière partie qui contient l’identifiant
        $this->setIdentif($id);  // ajoute l’identifiant à l’attribut identif
        $this->setType('vimeo');  // signale qu’il s’agit d’une video vimeo et l’inscrit dans l’attribut $type
    }

    //#[ORM\PrePersist()] // Les trois événements suivant s’exécutent avant que l’entité soit enregistrée
    //#[ORM\PreUpdate()]
    //#[ORM\PreFlush()]
    
    public function extractIdentif(): void
    {
        $url = $this->url;  // on récupère l’url

        if (preg_match("#^(http|https)://video.fdlibre.eu/#", $url))  // Si c'est peertube fdlibre
        {
            $this->peertubeId($url);
        }

        if (preg_match("#^(http|https)://www.youtube.com/#", $url))  // Si c’est une url Youtube on execute la fonction correspondante
        {
            $this->youtubeId($url);
        }
        if (preg_match("#^(http|https)://youtu.be/#", $url))  // Si c’est une url Youtube on execute la fonction correspondante
        {
            $this->youtubeCourteId($url);
        }
        else if((preg_match("#^(http|https)://www.dailymotion.com/#", $url))) // Si c’est une url Dailymotion on execute la fonction correspondante
        {
            $this->dailymotionId($url);
        }
        else if((preg_match("#^(http|https)://vimeo.com/#", $url))) // Si c’est une url Vimeo on execute la fonction correspondante
        {
            $this->vimeoId($url);
        }

    }

    public function embedUrl(): ?string
    {
        $control = $this->getType();  // on récupère le type de la vidéo
        $id = strip_tags($this->getIdentif()); // on récupère son identifiant

        if ($control == 'youtube') {
            $embed = "https://www.youtube-nocookie.com/embed/" . $id;
            return $embed;
        } else if ($control == 'dailymotion') {
            $embed = "https://www.dailymotion.com/embed/video/" . $id;
            return $embed;
        } else if ($control == 'vimeo') {
            $embed = "https://player.vimeo.com/video/" . $id;
            return $embed;
        } else if ($control == 'peertube') {
            $embed = "https://video.fdlibre.eu/videos/embed/" . $id;
            return $embed;
        } else {
            return null;
        }
    }

    public function url(): ?string
    {
        $control = $this->getType();  // on récupère le type de la vidéo
        $id = strip_tags($this->getIdentif()); // on récupère son identifiant

        if ($control == 'youtube') {
            $embed = "https://www.youtube.com/watch?v=" . $id;
            return $embed;
        } else if ($control == 'dailymotion') {
            $embed = "https://www.dailymotion.com/video/" . $id;
            return $embed;
        } else if ($control == 'vimeo') {
            $embed = "https://vimeo.com/" . $id;
            return $embed;
        } else if ($control == 'peertube') {
            $embed = "https://video.fdlibre.eu/videos/watch/" . $id;
            return $embed;
        } else {
            return null;
        }
    }


    public function image(): ?string
    {
        $control = $this->getType();  // on récupère le type de la vidéo
        $id = strip_tags($this->getIdentif()); // on récupère son identifiant

        if ($control == 'youtube') {
            $image = 'https://img.youtube.com/vi/' . $id . '/hqdefault.jpg';
            return $image;
        } else if ($control == 'dailymotion') {
            $image = 'https://www.dailymotion.com/thumbnail/150x120/video/' . $id . '';
            return $image;
        } else if ($control == 'vimeo') {
            $hash = unserialize(file_get_contents("https://vimeo.com/api/v2/video/" . $id . ".php"));
            $image = $hash[0]['thumbnail_small'];
            return $image;
        } else {
            return null;
        }
    }


    public function video(): string
    {
        $video = "<iframe src='".$this->embedUrl()."' allowfullscreen></iframe>";
        return $video;
    }
}