<?php

namespace App\Entity;

use App\Repository\FilmRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Table(name: "film")]
#[ORM\Entity(repositoryClass: FilmRepository::class)]
class Film
{
    #[ORM\Column(name: "id", type: "integer")]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "AUTO")]
    
    private ?int $id = null;

    #[ORM\Column(name: "titre", type: "string", length: 191)]
    
    private ?string $titre = null;

    #[ORM\Column(name: "annee", type: "date", nullable: true)]
    
    private ?\DateTimeInterface $annee = null;

    #[ORM\Column(name: "date_submited", type: "datetime", nullable: true)]
    
    private ?\DateTimeInterface $dateSubmited = null;

    #[ORM\Column(name: "lien", type: "string", length: 191, nullable: true)]
    #[Assert\Url()]
    
    private ?string $lien = null;

    #[ORM\Column(name: "note", type: "float", nullable: true)]
    
    private ?float $note = null;

    #[ORM\Column(name: "nb_coms", type: "integer", nullable: true)]
    
    private ?int $nbComs = null;

    #[Assert\Valid()]
    #[ORM\OneToOne(targetEntity: MediaVideo::class, cascade: ["persist","remove"])]
    #[ORM\JoinColumn(nullable: true)]
    
    private ?MediaVideo $mediaVideo = null;

    #[ORM\ManyToMany(targetEntity: Realisateur::class, inversedBy: "films", cascade: ["persist"])]
    
    private ?Collection $realisateurs = null;

    #[ORM\OneToMany(targetEntity: Commentaire::class, mappedBy: "film", orphanRemoval: true)]
    
    private ?Collection $commentaires = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: true)]
    
    private ?User $authered = null;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: "films")]
    #[ORM\JoinTable(name: "filmsavoir_users")]
    
    private ?Collection $usersWantToView = null;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: "filmsVus")]
    #[ORM\JoinTable(name: "filmsvus_users")]
    
    private ?Collection $usersWhoSeen = null;

    #[ORM\ManyToMany(targetEntity: Genre::class, cascade: ["persist"])]
    
    private ?Collection $genres = null;

    #[ORM\Column(type: "text", nullable: true)]
    
    private ?string $information = null;

    #[ORM\Column(type: "date", nullable: true)]
    
    private ?\DateTimeInterface $dateSortie = null;

    public function __construct()
    {
        $this->realisateurs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->usersWantToView = new \Doctrine\Common\Collections\ArrayCollection();
        $this->usersWhoSeen = new \Doctrine\Common\Collections\ArrayCollection();
        $this->genres = new \Doctrine\Common\Collections\ArrayCollection();
        $this->commentaires = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setDateSubmited(new \DateTime());
    }

    public function getAuthered(): ?User
    {
        return $this->authered;
    }

    public function setAuthered(?User $authered): self
    {
        $this->authered = $authered;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTitre(?string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setAnnee(?\DateTimeInterface $annee): self
    {
        $this->annee = $annee;

        return $this;
    }

    public function getAnnee(): ?\DateTimeInterface
    {
        return $this->annee;
    }

    public function getLien(): ?string
    {
        return $this->lien;
    }

    public function setLien(?string $lien = null): self
    {
        $this->lien = $lien;

        return $this;
    }

    public function getDateSubmited(): ?\DateTimeInterface
    {
        return $this->dateSubmited;
    }

    public function setDateSubmited(?\DateTimeInterface $dateSubmited): self
    {
        $this->dateSubmited = $dateSubmited;

        return $this;
    }

    public function isNew(): ?bool
    {
        $finNew = (new \DateTime($this->getDateSubmited()->format("Y/m/d")))->modify('+1 day');
        return (new \DateTime('now') < $finNew);
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        $this->commentaires[] = $commentaire;

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        $this->commentaires->removeElement($commentaire);

        return $this;
    }

    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addRealisateur(Realisateur $realisateur): self
    {
        $this->realisateurs[] = $realisateur;

        return $this;
    }

    public function removeRealisateur(Realisateur $realisateur):  self
    {
        $this->realisateurs->removeElement($realisateur);

        return $this;
    }

    public function getRealisateurs(): Collection
    {
        return $this->realisateurs;
    }

    public function addUserWantToView(User $user): self
    {
        $this->usersWantToView[] = $user;
        $user->addFilm($this);

        return $this;
    }

    public function removeUserWantToView(User $user): self
    {
        $this->usersWantToView->removeElement($user);
        $user->removeFilm($this);

        return $this;
    }

    public function getUsersWantToView(): Collection
    {
        return $this->usersWantToView;
    }

    public function addUserWhoSeen(User $user):self
    {
        $this->usersWhoSeen[] = $user;
        $user->addFilmVu($this);

        return $this;
    }

    public function removeUserWhoSeen(User $user): self
    {
        $this->usersWhoSeen->removeElement($user);
        $user->removeFilmVu($this);

        return $this;
    }

    public function getUsersWhoSeen(): Collection
    {
        return $this->usersWhoSeen;
    }

    public function getGenres(): Collection
    {
        return $this->genres;
    }

    public function addGenre(Genre $genre): self
    {
        $this->genres[] = $genre;
        return $this;
    }

    public function removeGenre(Genre $genre): self
    {
        $this->genres->removeElement($genre);
        return $this;
    }

    public function getNote(): ?float
    {
        return $this->note;
    }

    public function setNote(?float $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getNbComs(): ?int
    {
        return $this->nbComs;
    }

    public function setNbComs(?int $nbComs): self
    {
        $this->nbComs = $nbComs;

        return $this;
    }

    public function getInformation(): ?string
    {
        return $this->information;
    }

    public function setInformation(?string $information): self
    {
        $this->information = $information;

        return $this;
    }

    public function getDateSortie(): ?\DateTimeInterface
    {
        return $this->dateSortie;
    }

    public function setDateSortie(?\DateTimeInterface $dateSortie): self
    {
        $this->dateSortie = $dateSortie;

        return $this;
    }

    public function getMediaVideo(): ?MediaVideo
    {
        return $this->mediaVideo;
    }

    public function setMediaVideo(?MediaVideo $mediaVideo = null): self
    {
        $this->mediaVideo = $mediaVideo;

        return $this;
    }


}
