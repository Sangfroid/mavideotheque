<?php

namespace App\Entity;

use App\Repository\CommentaireRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: "commentaire")]
#[ORM\Entity(repositoryClass: CommentaireRepository::class)]
class Commentaire
{
    #[ORM\Column(name: "id", type: "integer")]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "AUTO")]

    private ?int $id = null;

    #[ORM\Column(name: "contenu", type: "text", length: 191, nullable: true)]
    
    private ?string $contenu = null;

    #[ORM\Column(name: "note", type: "integer", nullable: true)]

    private ?int $note = null;

    #[ORM\ManyToOne(targetEntity: Film::class, inversedBy: "commentaires")]
    #[ORM\JoinColumn(nullable: false)]

    private ?Film $film = null;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]

    private ?User $user = null;

    #[ORM\Column(type: "datetime", nullable: true)]
    private ?\DateTimeInterface $dateSubmitted = null;

    public function __construct ()
    {
        $this->dateSubmitted = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setContenu(?string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(?int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function setFilm(?Film $film): self
    {
        $this->film = $film;

        return $this;
    }

    public function getFilm(): ?Film
    {
        return $this->film;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getDateSubmitted(): ?\DateTimeInterface
    {
        return $this->dateSubmitted;
    }

    public function setDateSubmitted(?\DateTimeInterface $dateSubmitted): self
    {
        $this->dateSubmitted = $dateSubmitted;

        return $this;
    }
}
