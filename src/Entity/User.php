<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

#[ORM\Table(name: "user")]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[UniqueEntity(fields: "username", message: "Le username est déjà utilisé")]
#[UniqueEntity(fields: "mail", message: "L'email est déjà utilisé")]
#[UniqueEntity(fields: "token", message: "Erreur, token non unique")]

class User implements UserInterface,PasswordAuthenticatedUserInterface
{

    #[ORM\Column(name: "id", type: "integer")]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: "AUTO")]
    private ?int $id = null;

    #[ORM\Column(name: "username", type: "string", length: 191, unique: true)]
    private ?string $username = null;

    #[ORM\Column(name: "prenom", type: "string", length: 191, nullable: true)]
    private ?string $prenom = null;

    #[ORM\Column(name: "nom", type: "string", length: 191, nullable: true)]
    private ?string $nom = null;

    #[ORM\Column(name: "mail", type: "string", length: 191, unique: true)]
    #[Assert\Email()]
    private ?string $mail = null;

    #[ORM\Column(name: "password", type: "string", length: 191)]
    private ?string $password = null;

    #[ORM\Column(name: "token", type: "string", length: 191, unique: true)]

    private ?string $token = null;

    #[ORM\Column(name: "token_validity", type: "datetime")]
    #[Assert\Type("DateTime")]
    private ?\DateTimeInterface $tokenValidity = null;

    #[ORM\Column(name: "salt", type: "string", length: 191, nullable: true)]
    private ?string $salt = null;

    #[ORM\Column(name: "is_active", type: "boolean")]
    private ?bool $isActive = null;

    #[ORM\Column(name: "roles", type: "array")]
    private ?array $roles = array();

    #[ORM\ManyToMany(targetEntity: Film::class, mappedBy: "usersWantToView")]
    private ?Collection $films = null;

    #[ORM\ManyToMany(targetEntity: Film::class, mappedBy: "usersWhoSeen")]
    private ?Collection $filmsVus = null;

    #[ORM\OneToOne(targetEntity: Profile::class, mappedBy: "user", cascade: ["persist", "remove"])]
    private ?Profile $profile = null;

    #[ORM\Column(name: "last_activity", type: "datetime")]
    #[Assert\Type("DateTime")]
    private ?\DateTimeInterface $lastActivity = null;

    public function getLastActivity(): ?\DateTimeInterface
    {
        return $this->lastActivity;
    }

    public function setLastActivity(?\DateTimeInterface $lastActivity): self
    {
        $this->lastActivity = $lastActivity;

        return $this;
    }

    public function isActiveNow(): ?bool
    {
        $delay = new \DateTime('5 minutes ago');

        return ( $this->getLastActivity() > $delay );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getUsername(): ?string
    {
        return (string) $this->username;
    }

    public function getUserIdentifier(): string
    {
        return $this->username;
    }

    public function setActivated(?bool $activated): self
    {
        $this->isActive = $activated;

        return $this;
    }

    public function getActivated(): ?bool
    {
        return $this->isActive;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(?string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPassword() : ?string
    {
        return $this->password;
    }

    public function setSalt(?string $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    public function getSalt(): ?string
    {
        return $this->salt;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        //$this->token = hash("sha512", uniqid());
        $this->token = $token;
        $this->setTokenValidity(new \DateTime());

        return $this;
    }

    public function getTokenValidity(): ?\DateTimeInterface
    {
        return $this->tokenValidity;
    }

    private function setTokenValidity(?\DateTimeInterface $tokenValidity): self
    {
        $this->tokenValidity = $tokenValidity;

        return $this;
    }

    public function isValidToken(): ?bool
    {
        $expire = (new \DateTime($this->getTokenValidity()->format("Y-m-d H:i:s")))->modify('+1 hour');
        dump($expire);
        dump(new \DateTime('now'));
        return ( $expire > new \DateTime('now') );
    }

    public function setRoles(?array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        return array_unique($roles);

    }

    public function eraseCredentials(): void
    {

    }

    public function __construct()
    {
        $this->films = new \Doctrine\Common\Collections\ArrayCollection();
        //$this->setToken();
        $this->setLastActivity(new \DateTime('now'));
        $this->setActivated(true);
    }

    ///////////////////////////////////////////////////////////////
   
    public function addFilm(Film $film): self
    {
        $this->films[] = $film;

        return $this;
    }

    public function removeFilm(Film $film): self
    {
        $this->films->removeElement($film);

        return $this;
    }

    public function getFilms(): Collection
    {
        return $this->films;
    }

    ///////////////////////////////////////////////////////////////////////////

    public function addFilmVu(Film $film): self
    {
        $this->filmsVus[] = $film;

        return $this;
    }

    public function removeFilmVu(Film $film): self
    {
        $this->filmsVus->removeElement($film);

        return $this;
    }

    public function getFilmsVus(): Collection
    {
        return $this->filmsVus;
    }

    public function getNomComplet(): ?string
    {
        if ($this->prenom == null && $this->nom == null)
        {
            return $this->getUserIdentifier();
        } else
        return $this->getPrenom()." ".$this->getNom();
    }

    public function isAccountNonExpired(): ?bool
    {
        return true;
    }

    public function isAccountNonLocked(): ?bool
    {
        return true;
    }

    public function isCredentialsNonExpired(): ?bool
    {
        return true;
    }

    public function isEnabled(): ?bool
    {
        return $this->isActive;
    }

    public function serialize(): ?string
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
            // see section on salt below
            // $this->salt,
        ));
    }

    public function unserialize(?string $serialized):void 
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,

            // see section on salt below
            // $this->salt
            ) = unserialize($serialized, array('allowed_classes' => false));
    }

    public function wantToSee(Film $film): ?bool
    {
        return $this->getFilms()->contains($film);
    }

    public function haveSeen(Film $film): ?bool
    {
        return $this->getFilmsVus()->contains($film);
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(?Profile $profile): self
    {
        // set the owning side of the relation if necessary
        if ($profile->getUser() !== $this) {
            $profile->setUser($this);
        }

        $this->profile = $profile;

        return $this;
    }

}
