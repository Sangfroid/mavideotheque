<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class)
            ->add('password', RepeatedType::class, array(
                'type'          =>  PasswordType::class,
                'first_options' =>  array('label'   => 'password'),
                'second_options'=>  array('label'   => 'repeat-password'),
                'invalid_message'   =>  'Les mots de passe ne correspondent pas'
            ))
            ->add('prenom', TextType::class, array(
                'required'  => false
            ))
            ->add('nom', TextType::class, array(
                'required'  => false
            ))
            ->add('mail', EmailType::class)
            ->add('roles', ChoiceType::class, array(
                'choices'   => array (
                    'Administrateur'    =>  'ROLE_ADMIN',
                    'User'     =>  'ROLE_USER',
                    'Modérateur'   =>  'ROLE_MODERATEUR',
                    'Super Admin'   =>  'ROLE_SUPER_ADMIN'
                ),
                'multiple'  => true
            ))
            ->add('activated', CheckboxType::class, array(
                'label_attr' => [
                    'class' =>  'checkbox-switch'
                ],
                'required'  => false
            ))
            ->add('save', SubmitType::class, array('label' => 'Enregistrer'));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() : string
    {
        return 'App_user';
    }


}
