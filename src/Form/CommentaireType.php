<?php

namespace App\Form;

use App\Entity\Commentaire;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class CommentaireType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
	    $builder
            ->add('note', NumberType::class, [
                'required'  => false,
                'label'       => false,
                'attr'      => [
                    'min'   => 0,
                    'max'   => 5,
                    'class' => "rating",
                    'data-step' => 1,
                    'data-show-clear'   => "true",
                    'data-show-caption' => "false",
                    'data-size'         => "sm",
                    'data-theme'          => "krajee-fa"
                ]
            ])
            ->add('contenu', TextareaType::class, [
                'required' => false,
                'label' => false,
                'attr'  => [
                    'rows'  => 10
                ]
            ])
            ->add('save', SubmitType::class, [
		        'label' => 'Enregistrer le commentaire',
                'attr' => [
                    'class'  =>  'btn-primary'
                ]
            ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Commentaire::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() : string
    {
        return 'App_commentaire';
    }


}
