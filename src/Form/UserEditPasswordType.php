<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserEditPasswordType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('username')
            ->remove('nom')
            ->remove('prenom')
            ->remove('mail')
            ->remove('activated')
            ->remove('roles');
    }

    public function getParent() : ?string
    {
        return UserType::class;
    }
}
