<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class FilmType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	    $builder
            ->add('titre', TextType::class)
		    ->add('annee', DateType::class, array(
		        'widget'    => 'single_text',
                'label'     => 'Année',
                'years'     => range(1930, 2050, 1),
                'html5'     => false,
                'format'    =>  'yyyy',
                'required'  =>  false
            ))
            ->add('lien', TextType::class, array(
                'required'  => false
            ))
            ->add('mediaVideo', MediaVideoType::class,array(
                'required'  =>  false,
            ))
            ->add('dateSortie', DateType::class, [
                'html5'     =>  true,
                'widget'    =>  'single_text',
                'required'  =>  false
            ])
            ->add('information', TextareaType::class, array(
                'required' => false,
            ))
            ->add('genres', CollectionType::class, array(
                'entry_type'    =>  GenreType::class,
                'entry_options' =>  array('label'=>false),
                'allow_add'     =>  true,
                'allow_delete'  =>  true
            ))
            ->add('realisateurs', CollectionType::class, array(
                'entry_type'    =>  RealisateurType::class,
                'entry_options' =>  array('label'=>false),
                'allow_add'     =>  true,
                'allow_delete'  =>  true,
                'allow_extra_fields'    => true
            ))
		    ->add('save', SubmitType::class, array('label' => 'Enregistrer'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\Entity\Film'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() : string
    {
        return 'App_film';
    }


}
