<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use App\Service\Nouveautes;

class TwigNouveautes extends AbstractExtension
{
    public function getFunctions () : array
    {
        return array(
            new TwigFunction('afficheNouveautes', array(Nouveautes::class, 'afficher')),
        );
    }

    public function getName() : string
    {
        return 'afficheNouveautes';
    }


}