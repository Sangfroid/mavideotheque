<?php

namespace App\Command;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppUsersActivateCommand extends Command
{
    public function __construct(private UserRepository $repoUser, private EntityManagerInterface $em)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:users:activate')
            ->setDescription('Activer tous les users')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) :int
    {
        $io = new SymfonyStyle($input, $output);
        $question = new ConfirmationQuestion('Activer tous les utilisateurs ?', false);
        
        if (! $io->askQuestion($question))
        {
            $io->warning("Pas d'activation des users.");
            return COMMAND::INVALID;
        }
        $users = $this->repoUser->findAll();
        foreach ($users as $user)
        {
            $user->setActivated(true);
        }
        $this->em->flush();

        $io->success('Les utilisateurs sont activés');
        return COMMAND::SUCCESS;
    }

}
