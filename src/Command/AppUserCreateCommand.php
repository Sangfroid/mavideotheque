<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use App\Service\UserManager;
use Symfony\Component\Console\Style\SymfonyStyle;

class AppUserCreateCommand extends Command
{
    private $userManager;

    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:user:create')
            ->setDescription('Créer un utilisateur')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int
    {
        $io = new SymfonyStyle($input, $output);

        $username = $io->askQuestion(new Question('Nom (username) de l\'utilisateur '));
        $prenom = $io->askQuestion(new Question('Prénom de l\'utilisateur '));
        $nom = $io->askQuestion(new Question('Nom de l\'utilisateur '));
        $mail = $io->askQuestion(new Question('Email ', 'bidule@truc.chose'));
        $password = $io->askHidden("Mot de passe");
        $roles = $io->askQuestion((new ChoiceQuestion('Roles ', array('ROLE_SUPER_ADMIN', 'ROLE_ADMIN', 'ROLE_USER', 'ROLE_MODERATEUR'), '0'))->setMultiselect(true));

        $this->userManager->createUser(
            "$username",
            "$password",
            "$nom",
            "$prenom",
            "$mail",
            $roles,
            true);

        $io->success('Nom du pélo : '.$username);
        return COMMAND::SUCCESS;
    }

}
