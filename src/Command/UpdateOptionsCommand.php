<?php

namespace App\Command;

use App\Entity\Profile;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdateOptionsCommand extends Command
{    
    public function __construct(private EntityManagerInterface $em, private UserRepository $repoUser)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('app:update:options')
            ->setDescription('Commande pour rattacher une entité Profile aux users existants')
            ->addOption('force', null, InputOption::VALUE_NONE, 'Forcer la création des profils utilisateur')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if ($input->getOption('force')) {
            $users = $this->repoUser->findAll();
            foreach($users as $user) {
                if ($user->getProfile() === null)
                {
                    $io->note(sprintf('%s : pas de profil', $user->getNomComplet()));
                    $profil = new Profile();
                    $profil->setUser($user);
                    $this->em->persist($profil);
                }
            }
            $this->em->flush();

            $io->success(sprintf('La bdd a été modifiée.'));
            return Command::SUCCESS;
        } else {
            $io->note(sprintf('La bdd n\'a pas été modifiée. Utilisez l\'option --force.'));

            return Command::SUCCESS;
        }
    }
}
