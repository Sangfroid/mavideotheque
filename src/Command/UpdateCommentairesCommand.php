<?php

namespace App\Command;

use App\Repository\FilmRepository;
use App\Service\NoteMoyenne;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdateCommentairesCommand extends Command
{

    public function __construct(private NoteMoyenne $notesAndComs, private FilmRepository $repoFilm)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:commentaires:update')
            ->setDescription('Update les notes et nb coms')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) :int
    {
        $io = new SymfonyStyle($input, $output);
        $films = $this->repoFilm->findAll();
        foreach ($films as $film)
        {
            $this->notesAndComs->calculerMoyenne($film);
        }

        $io->success('Tous les coms et toutes les notes sont mis à jour');
        return COMMAND::SUCCESS;
    }

}
