<?php

namespace App\Controller;

use App\Entity\Realisateur;
use App\Form\RealisateurType;
use App\Repository\RealisateurRepository;
use App\Service\RealisateurManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RealisateurController extends AbstractController
{
    
    #[Route("/real", name: "realisateur_liste")]     
    public function indexAction(RealisateurRepository $repo) {
        $realisateurs = $repo->findAll();

        return $this->render('realisateur/liste.html.twig', array(
            'realisateurs'  => $realisateurs
        ));
    }

	
	#[Route("/real/ajouter", name: "realisateur_ajouter")]	 
	public function ajouterAction(Request $request, RealisateurManager $realisateurManager): Response
	{
		$realisateur = new Realisateur();
		$form = $this->createForm(RealisateurType::class, $realisateur);

		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid())
		{
			$realisateurManager->add($realisateur);
			$this->addFlash('success', 'Le réalisateur a été ajouté');
			return $this->redirectToRoute('realisateur_liste');
		}

		return $this->renderForm('realisateur/ajouter.html.twig', array(
			'form'	=>	$form
		));
	}

	
	#[Route("/real/modifier/{id}", name: "realisateur_modifier")]
	public function modifierAction(Request $request, Realisateur $realisateur, RealisateurManager $realisateurManager): Response
	{
		$form = $this->createForm(RealisateurType::class, $realisateur);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid())
		{
			$realisateurManager->edit($realisateur);
			$this->addFlash('success', 'Le réalisateur a été modifié');
			return $this->redirectToRoute('realisateur_liste');
		}

		return $this->renderForm('realisateur/modifier.html.twig', array(
			'form'	=> $form,
            'realisateur'   =>  $realisateur
		));
	}

	
	#[Route("/real/supprimer/{id}", name: "realisateur_supprimer")]
    #[IsGranted("ROLE_ADMIN")]
	public function supprimerAction(Request $request, Realisateur $realisateur, RealisateurManager $realisateurManager, FormFactoryInterface $ffi): Response
    {
		$form = $ffi->create();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $realisateurManager->del($realisateur);
            $this->addFlash('success', 'Le réalisateur '.$realisateur->getNomComplet().' a bien été supprimé.');
            return $this->redirectToRoute('realisateur_liste');
        }

		return $this->renderForm('realisateur/supprimer.html.twig', array(
			'realisateur'  =>  $realisateur,
            'form'  =>  $form
		));
	}

}
