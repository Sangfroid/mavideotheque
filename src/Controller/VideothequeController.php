<?php

namespace App\Controller;

use App\Entity\Commentaire;
use App\Entity\Realisateur;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Film;
use App\Form\CommentaireType;
use App\Form\FilmType;
use App\Form\RechercheTmdbType;
use App\Repository\CommentaireRepository;
use App\Repository\FilmRepository;
use App\Repository\GenreRepository;
use App\Repository\RealisateurRepository;
use App\Service\CommentaireManager;
use App\Service\FilmManager;
use App\Service\OptionsManager;
use App\Service\TmdbApiService;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class VideothequeController extends AbstractController
{
	#[Route("/", name: "videotheque_liste")]
	public function listeAction(Request $request, FilmRepository $repo, OptionsManager $options): Response
	{
        $listeFilms = $repo->findTous();

		return $this->render('videotheque/liste_'.$options->vue().'.html.twig', array(
            'listeFilms'	=>	$listeFilms,
            'titre'         =>  'Liste complète',
		));
    }

    #[Route("/prochaines-sorties", name: "prochaines_sorties")]
    public function prochainesSorties(Request $request, FilmRepository $repo, OptionsManager $options): Response
    {
        $listeFilms = $repo->findProchaines();

        return $this->render('videotheque/liste_'.$options->vue().'.html.twig', array(
            'listeFilms'	=>	$listeFilms,
            'titre'         =>  'Prochaines sorties',
		));
    }

    #[Route("/liste-by/{id}", name: "videotheque_listepargenre")]
	public function  listeParGenreAction(\App\Entity\Genre $genre, FilmRepository $repo, OptionsManager $options): Response
    {
        $films = $repo->findFilmWithGenre(array($genre->getName()));

        return $this->render('videotheque/liste_'.$options->vue().'.html.twig', array(
            'listeFilms'    => $films,
            'titre'         => 'Films par catégorie : '.$genre->getName()
        ));
    }

    #[Route("/liste-by_real/{id}", name: "videotheque_listeparreal")]
    public function  listeParRealisateurAction(Realisateur $realisateur, FilmRepository $repo, OptionsManager $options): Response
    {
        $films = $repo->findFilmWithReal(array($realisateur->getNomComplet()));

        return $this->render('videotheque/liste_'.$options->vue().'.html.twig', array(
            'listeFilms'    => $films,
            'titre'         => 'Films par réalisateur : '.$realisateur->getNomComplet()
        ));
    }

	#[Route("/ajouter/{idtmdb}", name: "videotheque_ajouter")]
	public function ajouterAction(Request $request, FilmManager $filmManager, TmdbApiService $tmdbApiService, string $idtmdb=null): Response
	{
        if ($idtmdb !== null) {
            $film = $tmdbApiService->hydrateFilm($idtmdb);
        } else {
    		$film = new Film;
        }
		$form = $this->createForm(FilmType::class, $film);

		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid())
		{
			$filmManager->addFilm($film);
			$this->addFlash('success', 'Le film a été ajouté');
			return $this->redirectToRoute('videotheque_voirfilm', array('id'=>$film->getId()));
		}

		return $this->renderForm('videotheque/ajouter.html.twig', array(
			'form'	=>	$form,
		));
	}

    #[Route("/addtmdb", name: "videotheque_ajouter_tmdb")]
    public function ajouterTmdb(Request $request, TmdbApiService $tmdbApiService): Response
    {
        $films = array();
        $nb = 0;
        $form = $this->createForm(RechercheTmdbType::class, null);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            if ($tmdbApiService->query($form->getData()['recherche'])) 
            {
                $nb = $tmdbApiService->countResults();
                $films = $tmdbApiService->getFilms();
            }
            
        }

        return $this->renderForm('videotheque/add_tmdb.html.twig', [
            'form'  => $form,
            'films'  => $films,
            'nb'    => $nb
        ]);
    }

	#[Route("/modifier/{id}", name: "videotheque_modifier")]
	public function modifierAction(Request $request, Film $film, FilmManager $filmManager): Response
	{
		$form = $this->createForm(FilmType::class, $film);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid())
		{
            $filmManager->editFilm($film);
			$this->addFlash('success', 'Le film a été modifié');
			return $this->redirectToRoute('videotheque_voirfilm',array('id'=>$film->getId()));
		}

		return $this->renderForm('videotheque/modifier.html.twig', array(
			'form'	=> $form,
		));
	}

	#[Route("/supprimer/{id}", name: "videotheque_supprimer")]
    #[IsGranted("ROLE_ADMIN")]
	public function supprimerAction(Request $request, Film $film, FilmManager $filmManager, FormFactoryInterface $ffi): Response
	{
        $form = $ffi->create();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $filmManager->delFilm($film);
            $this->addFlash('success', 'Le film '.$film->getTitre().' a bien été supprimé.');
            return $this->redirectToRoute('videotheque_liste');
        }

		return $this->renderForm('videotheque/supprimer.html.twig', array(
			'film'  =>  $film,
            'form'  =>  $form
		));
	}

	#[Route("/fichefilm/{id}", name: "videotheque_voirfilm")]
	public function voirFilmAction(Request $request, \App\Entity\Film $film, CommentaireManager $cm, Security $security, CommentaireRepository $repo): Response
    {
        if ($security->isGranted('IS_AUTHENTICATED_REMEMBERED'))
        {
            $commentaireUser = $repo->findOneBy(array('film'=>$film, 'user'=>$this->getUser()));
            if (is_null($commentaireUser))
            {
                $commentaireUser = new Commentaire();
            }
            $form = $this->createForm(CommentaireType::class, $commentaireUser);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid())
            {
                $cm->delEditAdd($commentaireUser, $film);
                $this->addFlash('success', 'Le commentaire a été posté');
                return $this->redirectToRoute('videotheque_voirfilm', array('id' => $film->getId()));
            }
        } else {
            $form = $this->createForm(CommentaireType::class, null);
        }
        
        return $this->renderForm('videotheque/voirfilm.html.twig', array(
            'film' => $film,
            'form'  =>  $form
        ));
    }

    #[Route("/ajax_req_realisateurs", name: "videotheque_ajax_realisateurs")]
    public function ajaxRealisateurs(RealisateurRepository $repo, SerializerInterface $serializer): Response
    {
        $realisateurs = $repo->findNomsComplets();
        $liste = array();
        foreach ($realisateurs as $key=>$nom)
        {
            $liste[$key] = $serializer->serialize($nom, 'json');
        }

        return new JsonResponse($liste);
    }

    #[Route("/ajax_req_genres", name: "videotheque_ajax_genres")]
    public function ajaxGenres(GenreRepository $repo, SerializerInterface $serializer): Response
    {
        $genres = $repo->findGenres();
        $liste = array();
        foreach ($genres as $key=>$nom)
        {
            $liste[$key] = $serializer->serialize($nom, 'json');
        }

        return new JsonResponse($liste);
    }
}
