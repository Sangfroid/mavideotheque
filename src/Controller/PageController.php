<?php

namespace App\Controller;

use App\Entity\Page;
use App\Form\PageType;
use App\Repository\PageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    #[Route('/p/{name}', name: 'app_page_show', methods: ['GET'])]
    public function showPage(PageRepository $pageRepository, string $name): Response
    {
        $page = $pageRepository->find($name);
        if (!$page) {
            throw $this->createNotFoundException('La page ' . $name . ' n\'existe pas.');
        }

        return $this->render('page/index.html.twig', [
            'page'   => $page
        ]);
    }

    #[Route('/p/edit/{name}', name: 'app_page_edit', methods: ['GET', 'POST'])]
    public function editPage(Request $request, Page $page, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(PageType::class, $page);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();

            $this->addFlash('success', 'Contenu modifié');
            return $this->redirectToRoute('app_page_show', ['name' => $page->getName()]);
        }

        return $this->renderForm('page/edit.html.twig', [
            'title' => $page->getName(),
            'form'  => $form
        ]);

    }
}
