<?php

namespace App\Controller;

use App\Repository\FilmRepository;
use App\Service\FilmManager;
use App\Service\OptionsManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class VideothequePersonnelleController extends AbstractController
{
	#[Route("/maliste/", name:"videothequepersonnelle_maliste")]
	public function maListeAction (Request $request, FilmRepository $repo, OptionsManager $options): Response
    {
        $films = $repo->findTousFavoritesByUser($this->getUser());

        return $this->render('videotheque/liste_'.$options->vue().'.html.twig', array(
            'listeFilms'    =>  $films,
            'titre'         =>  'Ma liste de films à voir'
        ));
    }

    #[Route("/maliste/modifieravoir/", name:"maliste_modifier_a_voir")]
    public function modifierFilmDansListeAction(Request $request, EntityManagerInterface $em, FilmRepository $repo, FilmManager $filmManager): Response
    {
        $result = null;
        $film = $repo->find($request->request->get('id_film'));
        if ($request->isXmlHttpRequest())
        {
            $result = $filmManager->inverseUserWantToView($film);
            $em->flush();
        }
        /*$resultat = $this->get('serializer')->serialize($film, 'json');*/
        return new JsonResponse((object)['newState' => $result]);
    }

    #[Route("/maliste/modifiervus/", name:"maliste_modifier_vus")]
    public function modifierFilmVusAction(Request $request, FilmRepository $repo, EntityManagerInterface $em, FilmManager $filmManager): Response
    {
        $result = null;

        $film = $repo->find($request->request->get('id_film'));
        if ($request->isXmlHttpRequest())
        {
            $result = $filmManager->inverseUserWhoSeen($film);
            $em->flush();
        }
        return new JsonResponse((object)['newState' => $result]);
    }

}
