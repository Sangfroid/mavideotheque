<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use App\Form\UserEditProfilType;
use App\Form\ProfileType;
use App\Repository\ProfileRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProfilController extends AbstractController
{
    
    #[Route("/profil", name: "user_profil")]    
    public function monProfilAction(Request $request, EntityManagerInterface $em): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(UserEditProfilType::class , $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $user = $form->getData();
            $em->flush();
            $this->addFlash('success', "Votre profil a été modifié");
        }

        return $this->renderForm('profil/monprofil.html.twig', array (
            'form'  =>  $form
        ));
    }

    #[Route("/preferences", name: "user_preferences")]
    public function mesPreferencesAction(Request $request, EntityManagerInterface $em, ProfileRepository $profileRepo): Response
    {
        $profile = $profileRepo->findByUser($this->getUser());

        $form = $this->createForm(ProfileType::class , $profile);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $profile = $form->getData();
            $em->flush();
            $this->addFlash('success', "Les préférences ont été modifiées");
        }

        return $this->renderForm('profil/mespreferences.html.twig', [
            'form'  =>  $form
        ]);
    }

}