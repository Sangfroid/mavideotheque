<?php

namespace App\Controller;

use App\Entity\Genre;
use App\Form\GenreType;
use App\Repository\FilmRepository;
use App\Repository\GenreRepository;
use App\Service\GenreManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GenreController extends AbstractController
{
    #[Route("/genre", name: "genre_liste")]
    public function indexAction(GenreRepository $repo): Response
	{
        $genres = $repo->findAll();

        return $this->render('genre/liste.html.twig', array(
            'genres'  => $genres
        ));
    }


	#[Route("/genre/ajouter", name: "genre_ajouter")]
	public function ajouterAction(Request $request, GenreManager $genreManager): Response
	{
		$genre = new Genre();
		$form = $this->createForm(GenreType::class, $genre);

		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid())
		{
			$genreManager->add($genre);
			$this->addFlash('success', 'Le genre a été ajouté');
			return $this->redirectToRoute('genre_liste');
		}

		return $this->renderForm('genre/ajouter.html.twig', array(
			'form'	=>	$form,
		));
	}

	 #[Route("/genre/modifier/{id}", name: "genre_modifier")]
	public function modifierAction(Request $request, Genre $genre, FilmRepository $repo, GenreManager $genreManager): Response
	{
		$form = $this->createForm(GenreType::class, $genre);
		$form->handleRequest($request);
        $films = $repo->findFilmWithGenre(array($genre->getName()));

		if ($form->isSubmitted() && $form->isValid())
		{
			$genreManager->edit($genre);
			$this->addFlash('success', 'Le genre a été modifié');
			return $this->redirectToRoute('genre_liste');
		}

		return $this->renderForm('genre/modifier.html.twig', array(
			'form'	=> $form,
            'genre'   =>  $genre,
            'films' => $films
		));
	}

	 #[Route("/genre/supprimer/{id}", name: "genre_supprimer")]
	public function supprimerAction(Request $request, Genre $genre, GenreManager $genreManager, FormFactoryInterface $ffi): Response
    {
        $form = $ffi->create();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $genreManager->del($genre);
            $this->addFlash('success', 'Le genre '.$genre->getName().' a bien été supprimé.');
            return $this->redirectToRoute('genre_liste');
        }

		return $this->renderForm('genre/supprimer.html.twig', array(
			'genre'  =>  $genre,
            'form'  =>  $form
		));
	}

}
