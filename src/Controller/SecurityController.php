<?php

namespace App\Controller;

use App\Entity\Profile;
use App\Form\UserEditPasswordType;
use App\Form\UserEditType;
use App\Form\UserRegisterType;
use App\Service\Mail;
use App\Service\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\UserType;
use App\Entity\User;
use App\Form\ProfileType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Response;


class SecurityController extends AbstractController
{
    #[Route("/login", name: "app_login")]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    #[Route("/logout", name: "app_logout")]
    public function logout()
    {
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }

    #[Route("/admin/createuser", name: "admin_createuser")]
    public function createUserAction(Request $request, UserManager $userManager): Response
    {
        $user = new User;
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $userManager->enregistrerUser($user);
            $this->addFlash('success', 'L\'utilisateur a été ajouté');
            return $this->redirectToRoute('admin_index');
        }
        return $this->renderForm('security/createuser.html.twig', array (
            'form'  => $form
        ));
    }

    #[Route("/admin/edituser/{id}", name: "admin_edituser")]
    public function editUserAction(Request $request, User $user, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(UserEditType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em->flush();
            $this->addFlash('success', 'L\'utilisateur a été modifié');
            return $this->redirectToRoute('admin_index');
        }
        return $this->renderForm('security/createuser.html.twig', array (
            'form'  => $form
        ));
    }

    #[Route("/admin/edituseroptions/{id}", name: "admin_edituseroptions")]
    public function editUseroptions (Request $request, Profile $options, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(ProfileType::class, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $em->flush();
            $this->addFlash('success', 'Les options de l\'utilisateur ont bien été modifiées.');

            return $this->redirectToRoute('admin_index');
        }
        
        return $this->renderForm('profil/preferences.html.twig', [
            'user'  => $options->getUser(),
            'form'  => $form
        ]);
    }

    #[Route("/admin", name: "admin_index")]
    public function indexAction(Request $request, UserRepository $repo): Response
    {
        $users = $repo->findAll();

        return $this->render('security/liste.html.twig', array(
            'users' => $users
        ));
    }

    #[Route("/admin/deluser/{id}", name: "admin_deluser")]
    public function delUserAction (Request $request, User $user, UserManager $userManager, FormFactoryInterface $ffi): Response
    {
        $form = $ffi->create();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $userManager->removeUser($user);
            $this->addFlash("success", "L'utilisateur a bien été supprimé");
            return $this->redirectToRoute('admin_index');
        }
        return $this->renderForm('security/supprimeruser.html.twig', array (
            'user'  =>  $user,
            'form'  =>  $form
        ));
    }

    #[Route("/register", name: "security_register")]
    public function registerAction(Request $request, UserManager $userManager, Mail $mail): Response
    {
        $user = new User();
        $form = $this->createForm(UserRegisterType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $userManager->register($user);

            //$adresse = $this->generateUrl('security_activate', array('token'=>$user->getToken()), UrlGeneratorInterface::ABSOLUTE_URL);
            //$mail->sendMailActivation($user, $adresse);
            
            $adresse = $this->generateUrl('admin_edituser', ['id' => $user->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
            $mail->sendMailDemandeActivation($user, $adresse);

            $this->addFlash('success', 'Votre compte a été créé. Un mail a été envoyé à l\'administrateur pour l\'activation du compte');
            return $this->redirectToRoute('security_attente');
        }

        return $this->renderForm('security/register.html.twig', [
            'form'  => $form
        ]);
    }

    #[Route("/attente", name: "security_attente")]
    public function attenteActivation(): Response
    {
        return $this->render('security/attente.html.twig');
    }

    #[Route("/motdepasseoublie", name: "security_envoyertoken")]
    public function recupMdpAction(Request $request, UserManager $userManager, Mail $mail, EntityManagerInterface $em, UserRepository $userRepo): Response
    {
        $form = $this->createFormBuilder()
            ->add('email', EmailType::class)
            ->add('envoyer-mail', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $userRepo->findOneBy(array('mail'=>$data['email']));
            if ($user != null)
            {
                $userManager->generateToken($user);
                $em->flush();
                $adresse = $this->generateUrl('security_resetpassword', array('token'=>$user->getToken()), UrlGeneratorInterface::ABSOLUTE_URL);

                $mail->sendMailTokenMp($user, $adresse);

                $this->addFlash('success', "Un mail vous a été envoyé à ".$user->getUsername()." pour récupérer le mot de passe");
                return $this->redirectToRoute('app_login');
            } else {
                $this->addFlash('warning', "Cet email n'existe pas");
            }
        }
        return $this->renderForm('security/chercheruserpourpassword.html.twig', array (
            'form'  =>  $form
        ));
    }

    #[Route("/resetpassword/token={token}", name: "security_resetpassword")]
    public function resetPasswordAction(Request $request, UserManager $userManager, User $user = null): Response
    {
        if ($user != null) {
            if ($user->isValidToken()) {
                $form = $this->createForm(UserEditPasswordType::class, $user);
                $form->handleRequest($request);
                if ($form->isSubmitted() && $form->isValid()) {
                    $userManager->resetPassword($user);
                    $this->addFlash('success', 'Mot de passe changé, veuillez vous connecter');
                    return $this->redirectToRoute('videotheque_liste');
                }
                return $this->renderForm('security/password.html.twig', array(
                    'form' => $form
                ));
            }
        }
        $this->addFlash('warning', "Ce token n'est plus valide");
        return $this->redirectToRoute('videotheque_liste');

    }

    #[Route("/admin/resettokens", name: "admin_resettokens")]
    public function resetTokensAction(UserManager $userManager, EntityManagerInterface $em, UserRepository $userRepo): Response
    {
        $users = $userRepo->findAll();
        foreach ($users as $user)
        {
            $userManager->generateToken($user);
        }
        $em->flush();
        return $this->redirectToRoute('admin_index');
    }

    #[Route("/activate/token={token}", name: "security_activate")]
    public function activateAction(Request $request, EntityManagerInterface $em, User $user = null): Response
    {
        if ($user != null)
        {
            if ($user->isValidToken())
            {
                $user->setActivated(true);
                $em->flush();
                $this->addFlash('success', "Votre compte est activé");
                return $this->redirectToRoute('app_login');
            }
        }
        $this->addFlash('warning', "Ce token n'est plus valide");
        return $this->redirectToRoute('videotheque_liste');
    }
}