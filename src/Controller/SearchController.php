<?php

namespace App\Controller;

use App\Service\OptionsManager;
use App\Service\Search;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{

    #[Route("/recherche", name: "search_recherche")]
    public function searchAction(Request $request, Search $filmSearch, OptionsManager $options): Response
    {
        $query = $request->query->get('q', "");
        return $this->render('videotheque/liste_'.$options->vue().'.html.twig', array(
            'listeFilms'    =>  $filmSearch->search($query),
            'titre'         =>  'Recherche '. $query,
            'query'         =>  $query
        ));
    }
}
