<?php

namespace App\Repository;

use App\Entity\Film;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Film|null find($id, $lockMode = null, $lockVersion = null)
 * @method Film|null findOneBy(array $criteria, array $orderBy = null)
 * @method Film[]    findAll()
 * @method Film[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

class FilmRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Film::class);
    }

    public function findFilm($id): Film
    {
        $qb = $this->createQueryBuilder('f');
        $query = $qb
            ->select('f.titre', 'f.annee')
            ->where('f.id = :id')
            ->setParameter('id', $id)
            ->getQuery();
        return $query->getSingleResult();
    }

    public function findFilmWithGenre(array $genreNames) : array
    {
        $qb = $this->createQueryBuilder('f');
        $qb
            ->innerJoin('f.genres', 'g')
            ->addSelect('f');

        $qb->where($qb->expr()->in('g.name', $genreNames));

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function findFilmWithReal(array $realisateursnames): array
    {
        $qb = $this->createQueryBuilder('f');
        $qb
            ->leftJoin('f.realisateurs', 'rea')
            ->addSelect('f')
            ->where($qb->expr()->in('rea.nomComplet', $realisateursnames));

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function findFilmWithRealLike($query): array
    {
        $qb = $this->createQueryBuilder('f');
        $qb
            ->innerJoin('f.realisateurs', 'r')
            ->addSelect('f');
        $qb
            ->where($qb->expr()->like('r.nomComplet', ':nomComplet'))
            ->setParameter('nomComplet', '%'.$query.'%');

        return $qb->getQuery()->getResult();

    }

    public function findFilmWithGenreLike($genreName): array
    {
        $qb = $this->createQueryBuilder('f');
        $qb
            ->innerJoin('f.genres', 'g')
            ->addSelect('f');

        $qb->where($qb->expr()->like('g.name', ':genreName'))
            ->setParameter('genreName', '%'.$genreName.'%');

        return $qb
            ->getQuery()
            ->getResult();
    }



    public function findTousDesc(): array
    {
        $qb = $this->createQueryBuilder('f');
        $qb
            ->select('f')
            ->orderBy('f.dateSubmited', 'DESC');

        return $qb
            ->getQuery()
            ->getResult();
    }

    public function findTous (): array
    {
        $qb = $this->createQueryBuilder('f')
            ->leftJoin('f.authered', 'aut')->addSelect('aut')
            ->leftJoin('f.genres', 'gen')->addSelect('gen')
            ->leftJoin('f.realisateurs', 'rea')->addSelect('rea')
            ->orderBy('f.dateSubmited', 'DESC');
        return $qb
            ->getQuery()
            ->getResult();
    }

    public function findTousFavoritesByUser($user): array
    {
        $qb = $this->createQueryBuilder('f');
        $qb
            ->leftJoin('f.authered', 'aut')->addSelect('aut')
            ->leftJoin('f.genres', 'gen')->addSelect('gen')
            ->leftJoin('f.realisateurs', 'rea')->addSelect('rea')
            ->leftJoin('f.usersWantToView', 'wan')
            ->where($qb->expr()->in('wan', ':user'))
            ->setParameter('user', $user)
            ->orderBy('f.dateSubmited', 'DESC');
        return $qb
            ->getQuery()
            ->getResult();
    }

    public function findProchaines(): array
    {
        $date = new \DateTime('now');
        $interval = new \DateInterval("P1D");
        $interval->invert = 1;
        $date->add($interval);
        $qb = $this->createQueryBuilder('f');
        $qb
            ->leftJoin('f.authered', 'aut')->addSelect('aut')
            ->leftJoin('f.genres', 'gen')->addSelect('gen')
            ->leftJoin('f.realisateurs', 'rea')->addSelect('rea')
            ->where('f.dateSortie > :date')
            ->setParameter('date', $date)
            ->orderBy('f.dateSortie', 'ASC');
        return $qb
            ->getQuery()
            ->getResult();
    }

    public function findFilmWithExistentTitre(string $titre): array
    {
        return $this->createQueryBuilder('f')
            ->leftJoin('f.realisateurs', 'r')->addSelect('r')
            ->where('f.titre = :titre')
            ->setParameter('titre', $titre)
            ->getQuery()
            ->getResult()
        ;
    }
}
