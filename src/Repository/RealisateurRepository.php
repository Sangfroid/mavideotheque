<?php

namespace App\Repository;

use App\Entity\Realisateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Realisateur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Realisateur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Realisateur[]    findAll()
 * @method Realisateur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */

class RealisateurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Realisateur::class);  
    }
    public function findByNom($nom)
    {
        $qb = $this->createQueryBuilder ('r');
        $expr = $qb->expr();
        $query = $qb
            ->select('r.id', 'r.nom', 'r.prenom')
            ->where(
                $expr->like('r.nom', ':nom'))
            ->setParameter('nom', $nom.'%')
            ->getQuery();
            
        return $query->getResult();
    }

    public function findByNomComplet($nomComplet)
    {
        $qb = $this->createQueryBuilder ('r');
        $expr = $qb->expr();
        $tableau = explode(' ', $nomComplet);

        if (count($tableau) == 1) {
            $premier = $tableau[0];
            $orPremier = $expr->orX(
                $expr->like('r.nom', ':premier'),
                $expr->like('r.prenom', ':premier')
            );
            $andExpression = $expr->andX($orPremier);
            $qb->setParameter('premier', $premier.'%');
        }

        if (count($tableau) > 1) {
            $premier = $tableau[0];
            $deuxieme = $tableau[1];

            $orPremier = $expr->orX(
                $expr->eq('r.nom', ':premier'),
                $expr->eq('r.prenom', ':premier')
            );

            $orDeuxieme = $expr->orX(
                $expr->like('r.nom', ':deuxieme'),
                $expr->like('r.prenom', ':deuxieme')
            );

            $andExpression = $expr->andX($orPremier, $orDeuxieme);

            $qb
                ->setParameter('premier', $premier)
                ->setParameter('deuxieme', $deuxieme.'%');
        }

        $query = $qb
            ->select('r.id', 'r.nom', 'r.prenom')
            ->where($andExpression)
            ->getQuery();

        return $query->getResult();
    }

    public function findNomsComplets ()
    {
        $qb = $this->createQueryBuilder('r');
        $query = $qb
            ->select('r.nomComplet')
            ->getQuery();

        return $query->getArrayResult();
    }
}
