<?php

namespace App\Service;

use App\Entity\Realisateur;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class RealisateurManager
 */
class RealisateurManager {

    public function __construct(protected EntityManagerInterface $em)
    {
    }

    public function add(Realisateur $realisateur): void
    {
        $this->em->persist($realisateur);
        $this->em->flush();
    }

    public function edit(Realisateur $realisateur): void
    {
        $this->em->flush();
    }

    public function del(Realisateur $realisateur): void
    {
        foreach ($realisateur->getFilms() as $film)
        {
            $film->removeRealisateur($realisateur);
        }
        $this->em->remove($realisateur);
        $this->em->flush();
    }
}