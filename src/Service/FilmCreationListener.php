<?php

namespace App\Service;

use App\Entity\Commentaire;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use App\Entity\Film;
use Symfony\Component\Config\Definition\Exception\Exception;

class FilmCreationListener
{
    public function __construct(protected Mattermost $mattermostSender)
    {
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        if (!$entity instanceof Film and !$entity instanceof Commentaire)
        {
            return;
        }

        try {
            if ($entity instanceof Film)
                $this->mattermostSender->sendNouveauFilm($entity);
            else
                $this->mattermostSender->sendNouveauCommentaire($entity->getUser(), $entity->getFilm());
        } catch (Exception $exception) {

        }
    }
}