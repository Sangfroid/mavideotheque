<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\Profile;
use Symfony\Component\Security\Core\Security;

class OptionsManager
{
    protected Profile $options;

    public function __construct(Security $security)
    {
        $this->options = new Profile;
        if ($security->getToken() != null) {
            $user = $security->getToken()->getUser();
            if ($user instanceof User) {
                $this->options = $user->getProfile();
            }
        }
    }

    public function vue(): string
    {
        if ($this->options->getView() === 0)
        {
            return "tableaux";
        } else {
            return "vignettes";
        }
    }
}
