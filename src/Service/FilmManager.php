<?php

namespace App\Service;

use App\Entity\Film;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class FilmManager
 */
class FilmManager {
    protected $user;

    public function __construct(
        protected EntityManagerInterface $em,
        protected UniciteCollections $uc,
        protected CommentaireManager $cm,
        Security $security
    )
    {
        $this->user = $security->getUser();
    }

    public function addFilm (Film $film): void
    {
        $film->setAuthered($this->user);
        $film = $this->uc->assureUniciteCollections($film);
        $this->em->persist($film);
        $this->em->flush();
    }

    public function editFilm(Film $film): void
    {
        $film = $this->uc->assureUniciteCollections($film);
        $this->em->flush();
    }

    public function delFilm(Film $film): void
    {
        $commentaires = $film->getCommentaires();
        foreach($commentaires as $commentaire)
        {
            $this->cm->delCommentaire($commentaire);
        }
        $this->em->remove($film);

        $this->em->flush();
    }

    public function inverseUserWhoSeen (Film $film): bool
    {
        if ($film->getUsersWhoSeen()->contains($this->user)) {
            $film->removeUserWhoSeen($this->user);
        } else {
            $film->addUserWhoSeen($this->user);
        }
        return $this->isSeen($film);
    }

    public function inverseUserWantToView (Film $film): bool
    {
        if ($film->getUsersWantToView()->contains($this->user)) {
            $film->removeUserWantToView($this->user);
        } else {
            $film->addUserWantToView($this->user);
        }
        return $this->isWantedToBeSeen($film);
    }

    public function isSeen(Film $film, User $user = null): bool
    {
        $user = $user ?: $this->user;
        return $film->getUsersWhoSeen()->contains($user);
    }

    public function isWantedToBeSeen(Film $film, User $user = null): bool
    {
        $user = $user ?: $this->user;
        return $film->getUsersWantToView()->contains($user);
    }
}