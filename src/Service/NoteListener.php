<?php

namespace App\Service;

use App\Entity\Commentaire;;
use Doctrine\Persistence\Event\LifecycleEventArgs;


class NoteListener
{
    public function __construct(protected NoteMoyenne $noteMoyenne)
    {
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $this->calculer($args);

    }

    public function postUpdate (LifecycleEventArgs $args): void
    {
        $this->calculer($args);
    }

    public function postRemove (LifecycleEventArgs $args): void
    {
        $this->calculer($args);
    }

    private function calculer (LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof Commentaire) {
            return;
        }

        $this->noteMoyenne->calculerMoyenne($entity->getFilm());
    }

}