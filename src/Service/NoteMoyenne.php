<?php

namespace App\Service;

use App\Entity\Film;
use Doctrine\Common\Util\Debug;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * Calculer moyenne pour un film
 */
class NoteMoyenne {

    public function __construct(protected EntityManagerInterface $em)
    {
    }

    public function calculerMoyenne (Film $film): void
    {
        $commentaires = $film->getCommentaires();

        $total = 0;
        $nb = 0;

        foreach ($commentaires as $commentaire) {
            if ($commentaire->getNote() > 0 && $commentaire->getNote() != null)
                $nb++;
            $total += $commentaire->getNote();
        }

        $note = 0;

        if ($nb > 0) {
            $note = $total / $nb;
        }
        if ($note == 0)
            $note = null;

        $film->setNote($note);
        $film->setNbComs(count($commentaires));
        try {
            $this->em->flush();
        } 
        
        catch (Exception $e)
        {
            dump($e);
        }
    }
}