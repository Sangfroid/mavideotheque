<?php

namespace App\Service;

use App\Repository\GenreRepository;
use App\Repository\RealisateurRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Film;

/**
 * Faire des recherches de films
 */
class UniciteCollections
{
    /**
     * Search constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(
        protected RealisateurRepository $repoReal,
        protected GenreRepository $repoGenre
    )
    {

    }

    /**
     * @param \App\Entity\Film $film
     * @return \App\Entity\Film
     */
    public function assureUniciteCollections(Film $film): Film
    {
        $film = $this->checkRealisateurs($film);
        $film = $this->checkGenres($film);
        return $film;
    }

    /**
     * @param \App\Entity\Film $film
     * @return \App\Entity\Film
     */
    protected function checkRealisateurs(Film $film): Film
    {
        $realisateurs = $film->getRealisateurs();
        foreach ($realisateurs as $realisateur)
        {
            if ($realisateur->getId() === null)
            {
                $recherche = $this->repoReal->findOneBy(array('nomComplet'=>$realisateur->getNomComplet()));
                if ($recherche !== null)
                {
                    $film->removeRealisateur($realisateur);
                    $film->addRealisateur($recherche);
                }
            }
        }

        return $film;
    }

    /**
     * @param \App\Entity\Film $film
     * @return \App\Entity\Film
     */
    protected function checkGenres(Film $film): Film
    {
        $genres = $film->getGenres();
        foreach ($genres as $genre)
        {
            if ($genre->getId() === null)
            {
                $recherche = $this->repoGenre->findOneBy(array('name'=>$genre->getName()));
                if ($recherche !== null)
                {
                    $film->removeGenre($genre);
                    $film->addGenre($recherche);
                }
            }
        }

        return $film;
    }
}