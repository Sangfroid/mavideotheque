<?php

namespace App\Service;


use App\Entity\Film;
use App\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Envoi notifs mattermost
 */
class Mattermost
{
    /**
     * Mattermost Manager
     */
    public function __construct(
        protected HttpClientInterface $mattermostClient,
        protected string $channelId,
        protected string $channelIdAdmin,
        protected UrlGeneratorInterface $router,
        protected RequestStack $requestStack
    )
    {
    }

    protected function SendNotif(string $message, bool $isChanAdmin = False) :void
    {
        try {
            $response = $this->mattermostClient->request(
                'POST',
                'api/v4/posts', [
                    'json'  => [
                        "channel_id" => ($isChanAdmin ? $this->channelIdAdmin : $this->channelId),
                        "message"    => $message
                    ]
                ]
            );
            // Appeler au moins une méthode pour déclencher l'exception en cas d'erreur réseau
            $response->getHeaders();

        } catch (TransportExceptionInterface $e) {
            $this->requestStack->getSession()->getFlashBag()->add('error', "L'envoi vers mattermost a échoué.");
        }
    }

    public function sendNouveauFilm(Film $film) :void
    {
        $message =
            ":new: **"
            .$film->getAuthered()->getUserIdentifier()
            ."** vient d'ajouter **"
            ."[".$film->getTitre()."]("
            .$this->router->generate('videotheque_voirfilm', ['id' => $film->getId()], UrlGeneratorInterface::ABSOLUTE_URL)
            .")** dans la [vidéothèque]("
            .$this->router->generate('videotheque_liste', [], UrlGeneratorInterface::ABSOLUTE_URL)
            .").";
        $this->SendNotif($message);
    }

    public function sendNouveauCommentaire(User $user, Film $film) :void
    {
        $message =
            ":new: **"
            .$user->getUserIdentifier()
            ."** vient d'ajouter un commentaire sur le film **"
            ."[".$film->getTitre()."]("
            .$this->router->generate('videotheque_voirfilm', ['id' => $film->getId()], UrlGeneratorInterface::ABSOLUTE_URL)
            .")** dans la [vidéothèque]("
            .$this->router->generate('videotheque_liste', [], UrlGeneratorInterface::ABSOLUTE_URL)
            .").";

        $this->SendNotif($message);
    }

    public function sendNewUser(string $userName) : void
    {
        $message =
        ":warning: Un utilisateur vient de s'enregistrer dans la [vidéothèque]("
        .$this->router->generate('videotheque_liste', [], UrlGeneratorInterface::ABSOLUTE_URL)
        .")** : "
        .$userName
        ."**. Accéder à la [liste des utilisateurs]("
        .$this->router->generate('admin_index', [],  UrlGeneratorInterface::ABSOLUTE_URL)
        .").";

        $this->sendNotif($message, True);
    }


}