<?php

namespace App\Service;

use App\Entity\Profile;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

/**
 * Agir sur les utilisateurs
 */
class UserManager
{
    /**
     * UserManager constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(
        protected EntityManagerInterface $em,
        protected UserPasswordHasherInterface $passwordEncoder,
        protected TokenGeneratorInterface $tokenGenerator
    )
    {

    }

    public function createUser(string $username, string $password, string $nom, string $prenom, string $mail, array $roles, bool $activated): void
    {
        $user = new User();
        $options = new Profile();
        $user->setUsername($username);
        $user->setPrenom($prenom);
        $user->setNom($nom);
        $user->setMail($mail);
        $user->setRoles($roles);
        $user->setActivated($activated);
        $user->setPassword($password);
        $user->setProfile($options);
        $this->enregistrerUser($user);
    }

    public function register(User $user): void
    {
        $user->setActivated(false);
        $user->setRoles(array('ROLE_USER'));
        $this->enregistrerUser($user);
    }


    public function resetPassword(User $user): void
    {
        $user->setActivated($user);
        $this->enregistrerUser($user);
    }
    public function enregistrerUser (User $user): void
    {
        $encoded = $this->passwordEncoder->hashPassword($user, $user->getPassword());
        $user->setPassword($encoded);
        $this->generateToken($user);
        if ($user->getProfile() === null)
        {
            $options = new Profile();
            $user->setProfile($options);
        }
        $this->em->persist($user);
        $this->em->flush();
    }

    public function editUser (): void
    {
        $this->em->flush();
    }

    public function removeUser(User $user): void
    {
        $this->em->remove($user);
        $this->em->flush();
    }

    public function generateToken(User $user): void
    {
        $user->setToken($this->tokenGenerator->generateToken());
    }

}