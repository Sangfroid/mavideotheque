<?php

namespace App\Service;

use App\Entity\Commentaire;
use App\Entity\Film;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class CommentaireManager
 */
class CommentaireManager {

    protected ?UserInterface $user;

    public function __construct(
        protected EntityManagerInterface $em,
        protected UniciteCollections $uc,
        Security $security
    )
    {
        $this->user = $security->getUser();
    }

    public function addCommentaire (Commentaire $commentaire, Film $film): void
    {
        $commentaire->setContenu(htmlspecialchars($commentaire->getContenu()));
        $commentaire->setUser($this->user);
        $commentaire->setFilm(($film));
        $film->addCommentaire($commentaire);
        $this->em->persist($commentaire);
        $this->em->flush();
    }

    public function editCommentaire(Commentaire $commentaire): void
    {
        $commentaire->setContenu(htmlspecialchars($commentaire->getContenu()));
        $this->em->flush();
    }

    public function delCommentaire(Commentaire $commentaire): void
    {
        $commentaire->getFilm()->removeCommentaire($commentaire);
        $this->em->remove($commentaire);
        $this->em->flush();
    }

    private function isEmpty (Commentaire $commentaire): bool
    {
        return (
            is_null($commentaire->getContenu()) &&
            is_null($commentaire->getNote()));
    }

    public function delEditAdd(Commentaire $commentaire, Film $film): void
    {
        if (!$this->isEmpty($commentaire))
        {
            if (!is_null($commentaire->getId()))
            {
                $this->editCommentaire($commentaire);
                return;
            } else {
                $this->addCommentaire($commentaire, $film);
                return;
            }
        } else {
            if (!is_null($commentaire->getId()))
            {
                $this->delCommentaire($commentaire);
                return;
            }
        }

        /*if ($this->isEmpty($commentaire)) {
            if (!is_null($commentaire->getId())) {
                $this->delCommentaire($commentaire);
                return;
            } else {
                $this->em->flush();
                return;
            }
        } else {
            $cm->addCommentaire($commentaireUser, $film);
            return;
        }*/
    }
}