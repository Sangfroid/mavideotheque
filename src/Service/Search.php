<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Repository\FilmRepository;

/**
 * Faire des recherches de films
 */
class Search
{
    /**
     * Search constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(
        protected EntityManagerInterface $em,
        protected FilmRepository $repoFilm
    )
    {

    }

    /**
     * @param string $query
     * @return \App\Entity\Film[]
     */
    public function search(string $query): array
    {
        return \array_merge(
            $this->searchByFilm($query),
            $this->searchByRealisateur($query),
            $this->searchByGenre($query)
        );
    }

    /**
     * @param $query
     * @return array
     */
    protected function searchByFilm(string $query): array
    {
        $q = $this->em
            ->createQuery('SELECT f from App:Film f WHERE f.titre like :titre')
            ->setParameter('titre', '%'.$query.'%')
            ->getResult();
        return $q;
    }

    /**
     * @param $query
     * @return array
     */
    protected function searchByRealisateur(string $query): array
    {
        return $this->repoFilm->findFilmWithRealLike($query);
    }

    /**
     * @param $query
     * @return array
     */
    protected function searchByGenre(string $query): array
    {
        return $this->repoFilm->findFilmWithGenreLike($query);
    }
}