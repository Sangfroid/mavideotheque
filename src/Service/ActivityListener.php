<?php
namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\Security\Core\Security;

class ActivityListener
{
    public function __construct(protected EntityManagerInterface $em, protected Security $security)
    {
    }

    public function onKernelController(ControllerEvent $event): void
    {
        // Check that the current request is a "MASTER_REQUEST"
        // Ignore any sub-request
        if (!$event->isMainRequest()) {
            return;
        }

        // Check token authentication availability
        if ($this->security->getToken()) {
            $user = $this->security->getToken()->getUser();

            if ( ($user instanceof User) && !($user->isActiveNow()) ) {
                $user->setLastActivity(new \DateTime());
                $this->em->flush();
            }
        }
    }
}