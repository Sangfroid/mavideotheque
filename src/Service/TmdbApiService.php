<?php

namespace App\Service;

use App\Entity\Film;
use App\Entity\Genre;
use App\Entity\MediaVideo;
use App\Entity\Realisateur;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class TmdbApiService
{
    protected string $content = '';
    protected int $nbResults = 0;
    protected array $films = [];

    public function __construct(
        protected HttpClientInterface $tmdbClient,
        protected UniciteCollections $uc
    )
    {
        
    }

    public function query(string $titre): bool
    {
        try {
            $response = $this->tmdbClient->request(
                'GET',
                'search/movie',[
                    'query' => [                
                        'query' => $titre
                    ]
                ]
            );

            $this->content = $response->getContent();
            $this->jsonDecode();
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    public function getDetailMovie(string $id): ?string
    {
        $detail = null;
        try {
            $response = $this->tmdbClient->request(
                'GET',
                'movie/'. $id ,[
                    'query' => [
                        'append_to_response'    => 'credits,videos',
                    ]
                ]
            );
            $detail = $response->getContent();

        } catch (\Exception $e) {
            return null;
        }
        return $detail;
    }

    public function getFilms(): array
    {
        return $this->films;
    }

    public function countResults(): int
    {
        return $this->nbResults;
    }

    protected function jsonDecode(): void
    {
        $json = json_decode($this->content);

        $this->nbResults = $json->total_results;
        $entrees = $json->results;

        foreach($entrees as $entree) {
            $film = $this->hydrateFilm($entree->id);

            $this->films[$entree->id] = $film;

        }
    }

    public function hydrateFilm($filmTmdbId): Film
    {
        $filmTmdb = json_decode($this->getDetailMovie($filmTmdbId));
        $film = new Film();
        $film
            ->setTitre($filmTmdb->title)
            ->setInformation($filmTmdb->overview);

        if (preg_match ("/^\d{4}-\d{2}-\d{2}$/", $filmTmdb->release_date))
        {
            $dateSortie = $filmTmdb->release_date;
            $annee = substr($dateSortie, 0, 4) . '-01-01';
            $film
                ->setDateSortie(new \DateTimeImmutable($dateSortie))
                ->setAnnee(new \DateTimeImmutable($annee))
            ;

        } elseif (preg_match("/^\d{4}/", $filmTmdb->release_date)) {
            $annee = substr($filmTmdb->release_date, 0, 4) . '-01-01';
            $film                    
                ->setAnnee(new \DateTimeImmutable($annee));
            ;
        }

        ;
        foreach ($filmTmdb->genres as $genre) {
            $film->addGenre((new Genre())->setName($genre->name));
        }

        foreach ($this->getRealisateurs($filmTmdb) as $realisateur) {
            $film->addRealisateur($realisateur);
        }

        $videoTmdb = $this->getOneVideo($filmTmdb);
        if (!empty($videoTmdb)) {
            $video = new MediaVideo();
            $video->setFromTmdb($videoTmdb['type'], $videoTmdb['identif']);
            $film->setMediaVideo($video);
        }

        $film = $this->uc->assureUniciteCollections($film);
        return $film;
    }

    public function getRealisateurs($detailMovie): array
    {
        $directors = array();
        foreach($detailMovie->credits->crew as $crew) {
            if ($crew->job === 'Director') {
                $realisateur = new Realisateur();
                $realisateur->setNomComplet($crew->name);
                $directors[] = $realisateur;
            }
        }
        return $directors;
    }

    public function getOneVideo($detailMovie): array
    {
        foreach($detailMovie->videos->results as $video) {
            if (
                ($video->site === "YouTube" || $video->site === "Vimeo" || $video->site === "DailyMotion") &&
                $video->type === "Trailer" &&
                $video->iso_639_1 === "fr" && $video->iso_3166_1 === "FR"
            ) {
                return [
                    'type' => strtolower($video->site),
                    'identif'   => $video->key
                ];
            }
        }
        return [];
    }

}
