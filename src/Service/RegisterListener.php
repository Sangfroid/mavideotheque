<?php

namespace App\Service;

use App\Entity\User;
use Doctrine\Persistence\Event\LifecycleEventArgs;


class RegisterListener
{
    public function __construct(protected Mattermost $mattermostSender)
    {
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        if (!$entity instanceof User) {
            return;
        }

        $this->mattermostSender->sendNewUser($entity->getUserIdentifier());


    }
}