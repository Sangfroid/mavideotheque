<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Twig\Environment;

/**
 * Envoi de mails
 */
class Mail
{
    /**
     * Mail Manager
     * @param $mailer
     * @param $templating
     * @param $from
     * @param $reply
     * @param $name
     */
    public function __construct(
        protected MailerInterface $mailer,
        protected Environment $templating,
        protected string $from,
        protected string $reply,
        protected string $name
    )
    {

    }

    protected function sendMessage(string $subject, string $to, string $body): void
    {
        $mail = (new Email())
            ->from(new Address($this->from, $this->name))
            ->to($to)
            ->subject($subject)
            ->text($body)
            ->replyTo(new Address($this->reply, $this->name));

        $this->mailer->send($mail);
    }

    public function sendMailActivation(User $user, string $lien): void
    {
        $subject = "Activation de votre compte";
        $template = 'security/mail_activate.html.twig';
        $to = $user->getMail();
        $body = $this->templating->render($template, ['user' => $user, 'lien'  => $lien]);
        $this->sendMessage($subject, $to, $body);
    }

    public function sendMailDemandeActivation(User $user, string $lien): void
    {
        $subject = "Demande d'activation";
        $template = 'security/mail_demande_activation.html.twig';
        $to = $this->from;
        $body = $this->templating->render($template, ['user' => $user, 'lien' => $lien]);
        $this->sendMessage($subject, $to, $body);
    }

    public function sendMailTokenMp(User $user, string $lien): void
    {
        $subject = "Mot de passe perdu";
        $template = 'security/mail_tokenmdp.html.twig';
        $to = $user->getMail();
        $body = $this->templating->render($template, ['user' => $user, 'lien'  => $lien]);
        $this->sendMessage($subject, $to, $body);
    }
}