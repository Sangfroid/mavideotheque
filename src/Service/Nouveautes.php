<?php

namespace App\Service;

use App\Repository\CommentaireRepository;
use Twig\Environment;
use Twig\Extension\RuntimeExtensionInterface;

class Nouveautes implements RuntimeExtensionInterface
{

    public function __construct(
        protected CommentaireRepository $repoCommentaire,
        protected Environment $twig
    )
    {
    }

    public function afficher(): string
    {
        $commentaires = $this->repoCommentaire->findLast(5);

        return $this->twig->render('nouveautes/nouveautes.html.twig', [
            'commentaires'  =>  $commentaires
        ]);
    }
}