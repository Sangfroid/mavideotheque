<?php

namespace App\Service;

use App\Entity\Genre;
use App\Entity\Film;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class GenreManager
 */
class GenreManager {

    public function __construct(protected EntityManagerInterface $em)
    {
    }

    public function add(Genre $genre): void
    {
        $this->em->persist($genre);
        $this->em->flush();
    }

    public function edit(Genre $genre): void
    {
        $this->em->flush();
    }

    public function del(Genre $genre): void
    {
        $films = $this->em->getRepository(Film::class)->findFilmWithGenre(array($genre->getName()));
        foreach ($films as $film)
        {
            $film->removeGenre($genre);
        }
        $this->em->remove($genre);
        $this->em->flush();
    }
}