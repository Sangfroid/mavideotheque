<?php

namespace App\Tests\Entity;

use App\Entity\Genre;
use PHPUnit\Framework\TestCase;

class GenreTest extends TestCase
{
    public function testDefault(): void
    {
        $genre = new Genre();
        $genre->setName('Horreur');
        $this->assertSame('Horreur', $genre->getName());
        $this->assertNull($genre->getId());
    }
}
