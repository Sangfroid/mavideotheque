<?php

namespace App\Tests\Entity;

use App\Entity\Film;
use App\Entity\Realisateur;
use PHPUnit\Framework\TestCase;

class RealisateurTest extends TestCase
{
    public function testGetters(): void
    {
        $realisateur = new Realisateur();
        $realisateur->setNomComplet('Bill Bilbao');
        $film = new Film();
        $realisateur->addFilm($film);
        $this->assertEquals('Bill Bilbao', $realisateur->getNomComplet());
        $this->assertCount(1, $realisateur->getFilms());
        $this->assertContainsOnlyInstancesOf(Film::class, $realisateur->getFilms());
        $this->assertNull($realisateur->getId());
        $realisateur->removeFilm($film);
        $this->assertCount(0, $realisateur->getFilms());
    }
}
