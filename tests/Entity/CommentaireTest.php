<?php

namespace App\Tests\Entity;

use App\Entity\Commentaire;
use App\Entity\Film;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class CommentaireTest extends TestCase
{
    public function testDefault(): void
    {
        $film = new Film();
        $film->setTitre("Le bon, la brute et le truand");
        $user = new User();
        $user->setUsername('sangfroid');

        $comment = new Commentaire();
        $date = new \DateTime('now');
        
        $comment->setContenu('Un beau commentaire');
        $comment->setNote(4);
        $this->assertTrue( ($date->modify('-5 minute') < $comment->getDateSubmitted() && $comment->getDateSubmitted() < $date->modify('+5 minute')));
        $this->assertNull($comment->getId());
        $this->assertSame('Un beau commentaire', $comment->getContenu());
        $this->assertEquals(4, $comment->getNote());

        $comment->setDateSubmitted($date);
        $this->assertSame($date, $comment->getDateSubmitted());

        $comment->setFilm($film);
        $comment->setUser($user);

        $this->assertSame('Le bon, la brute et le truand', $comment->getFilm()->getTitre());
        $this->assertSame('sangfroid', $comment->getUser()->getUserIdentifier());
    }
}
