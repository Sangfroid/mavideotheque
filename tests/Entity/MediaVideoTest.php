<?php
declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\MediaVideo;
use PHPUnit\Framework\TestCase;

class MediaVideoTest extends TestCase
{
    /**
     * @dataProvider urlsForMedia
     */
    public function testByUrl(
        string $url,
        string $expectedUrl,
        string $expectedEmbedUrl,
        string $expectedType,
        string $expectedIdentif,
        bool $isPathImageExists
    ): void
    {
        $media = new MediaVideo();
        $media->setUrl($url);
        $this->assertEquals($expectedUrl, $media->getUrl());
        $this->assertEquals($expectedEmbedUrl, $media->embedUrl());
        $this->assertEquals($expectedType, $media->getType());
        $this->assertEquals($expectedIdentif, $media->getIdentif());
        $this->assertNull($media->getId());
        $this->assertEquals("<iframe src='".$expectedEmbedUrl."' allowfullscreen></iframe>", $media->video());
        if ($isPathImageExists) {
            $this->assertIsString($media->image());
        } else {
            $this->assertNull($media->image());
        }
    }

    /**
     * @dataProvider urlsForMedia
     */
    public function testFromTmdb(
        string $url,
        string $expectedUrl,
        string $expectedEmbedUrl,
        string $expectedType,
        string $expectedIdentif,
        bool $isPathImageExists
    ): void
    {
        $media2 = new MediaVideo();
        $media2->setFromTmdb($expectedType, $expectedIdentif);
        $this->assertEquals($expectedUrl, $media2->getUrl());
        $this->assertEquals($expectedEmbedUrl, $media2->embedUrl());
        $this->assertEquals($expectedType, $media2->getType());
        $this->assertEquals($expectedIdentif, $media2->getIdentif());
        $this->assertNull($media2->getId());
        $this->assertEquals("<iframe src='".$expectedEmbedUrl."' allowfullscreen></iframe>", $media2->video());
        if ($isPathImageExists) {
            $this->assertIsString($media2->image());
        } else {
            $this->assertNull($media2->image());
        }

    }


    public function urlsForMedia()
    {
        return [
            [
                'https://www.youtube.com/watch?v=jZm8eSX7MTc',
                'https://www.youtube.com/watch?v=jZm8eSX7MTc',
                'https://www.youtube-nocookie.com/embed/jZm8eSX7MTc',
                'youtube',
                'jZm8eSX7MTc',
                true
            ],
            [
                'https://www.dailymotion.com/video/x8efziw',
                'https://www.dailymotion.com/video/x8efziw',
                'https://www.dailymotion.com/embed/video/x8efziw',
                'dailymotion',
                'x8efziw',
                true
            ],
            [
                'https://vimeo.com/48130434',
                'https://vimeo.com/48130434',
                'https://player.vimeo.com/video/48130434',
                'vimeo',
                '48130434',
                true
            ],
            [
                'https://video.fdlibre.eu/videos/watch/eb38ccf6-62ce-4b52-874c-beaacc896612',
                'https://video.fdlibre.eu/videos/watch/eb38ccf6-62ce-4b52-874c-beaacc896612',
                'https://video.fdlibre.eu/videos/embed/eb38ccf6-62ce-4b52-874c-beaacc896612',
                'peertube',
                'eb38ccf6-62ce-4b52-874c-beaacc896612',
                false
            ],
            [
                'https://youtu.be/jZm8eSX7MTc',
                'https://www.youtube.com/watch?v=jZm8eSX7MTc',
                'https://www.youtube-nocookie.com/embed/jZm8eSX7MTc',
                'youtube',
                'jZm8eSX7MTc',
                true
            ],

        ];
    }

}
